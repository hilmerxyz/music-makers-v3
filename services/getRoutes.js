const { createClient } = require('contentful')

const client = createClient({
  space: 'yuuu9sjuzz8b',
  accessToken: '23200258709075e6196c31d294bdb06a754181fa6cfd9617696c29e5f8e464ae',
})


const getRoutes = async () => {
  let commercials = await client.getEntry('1OLI09dVKEIA84UEmCiOAK')
    .then((response) => {
      return response.fields.projects.map(item => item.fields.slug )
    })
    .catch(console.error)

  let films = await client.getEntry('DvPlfgEzaCCYSI6SquwSi')
    .then((response) => {
      return response.fields.projects.map(item => `film/${item.fields.slug}` )
  })
  .catch(console.error)

  return [...commercials, ...films]
}


module.exports = {
  getRoutes
}


