const pkg = require('./package')
const { getRoutes } = require('./services/getRoutes')


module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: 'Music Makers - Music Production Stockholm, Sweden',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Music production company. Stockholm, Sweden' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://cdn.polyfill.io/v2/polyfill.min.js?features=IntersectionObserver' },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/app.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src:'~/plugins/clazy-load.js', ssr: true},
    { src:'~/plugins/vue-mq.js', ssr: false},
    { src:'~/plugins/ga.js', ssr: false },
    { src:'~/plugins/vue-scroll.js', ssr: true},
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    'nuxt-sass-resources-loader',
    '@nuxtjs/style-resources'
  ],


  styleResources: {
    scss: [
      '@/assets/css/fonts.scss',
      '@/assets/css/colors.scss',
      '@/assets/css/media-queries.scss'
    ],
    // hoistUseStatements: true  // Hoists the "@use" imports. Applies only to "sass", "scss" and "less". Default: false.
  },

  sassResources: [
  ],

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    routes() {
      return getRoutes()
    }
  }
}
