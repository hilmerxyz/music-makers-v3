import Vuex from 'vuex'
import { createClient } from 'contentful'

const client = createClient({
  space: 'yuuu9sjuzz8b',
  accessToken: '23200258709075e6196c31d294bdb06a754181fa6cfd9617696c29e5f8e464ae',
});

const createStore = () => {
  return new Vuex.Store({
    state: {
      commercialProjects: [],
      filmProjects: []
    },
    mutations: {
      setCommercialProjects(state, payload) {
        return state.commercialProjects = payload
      },
      setFilmProjects(state, payload) {
        return state.filmProjects = payload
      }
    },
    getters: {
      getCommercialProjects(state) {
        return state.commercialProjects
      },
      getFilmProjects(state) {
        return state.filmProjects
      }

    },
    actions: {
      fetchCommercialProjects(ctx) {
        return client.getEntry('1OLI09dVKEIA84UEmCiOAK')
          .then((response) => {
            response.fields.projects
            let projects = response.fields.projects.map(item => {
              return {
                image: item.fields.image.fields.file.url,
                title: item.fields.title,
                director: item.fields.director,
                productionCompany: item.fields.productionCompany,
                video: item.fields.video,
                slug: item.fields.slug,
              }
            })
            ctx.commit('setCommercialProjects', projects)
          })
          .catch(console.error)
      },
      fetchFilmProjects(ctx) {
        return client.getEntry('DvPlfgEzaCCYSI6SquwSi')
          .then((response) => {
            response.fields.projects
            let projects = response.fields.projects.map(item => {
              return {
                image: item.fields.image.fields.file.url,
                title: item.fields.title,
                director: item.fields.director,
                productionCompany: item.fields.productionCompany,
                video: item.fields.video,
                slug: item.fields.slug,
              }
            })
            ctx.commit('setFilmProjects', projects)
          })
          .catch(console.error)
      },
    }
  })
}

export default createStore
